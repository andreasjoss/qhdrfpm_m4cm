.PS
include(pstricks.m4)
include(libcct.m4)
include(darrow.m4)

scale=25.4

maxpswid=12

define(`V_tick',`[line color "gray" up _hm/15]' )
define(`H_tick',`[line color "gray" right _hm/15]' )

define(`centreline',`rpoint_(`$1')define(`m4opts',`$2')
  define(`m4h',`ifelse(`$3',,dashwid,abs(`$3'))')dnl
  define(`m4v',`ifelse(`$4',,m4h/2,abs(`$4'))')dnl
  if (m4h+m4v)==0 then { m4n = 1 } \
  else { m4n = max(1,int(abs((rp_len)/(m4h*2+m4v*3)))) }
  m4f = (rp_len-(m4n -1)*m4v*3)/m4n
  for m4ti=1 to m4n do {
    line m4opts to rvec_(m4f,0)
    if (m4ti<m4n) then {move to rvec_(m4v,0)}
    line m4opts to rvec_(m4v,0)
    if (m4ti<m4n) then {move to rvec_(m4v,0)}
    } ')

define(`I_cross',`[\
  I: circle rad radius_ fill 1;
  line from 1/5 between I.ne and I.sw to 4/5 between I.ne and I.sw;
  line from 1/5 between I.nw and I.se to 4/5 between I.nw and I.se]' )

define(`I_dot',`[\
  I: circle rad radius_ fill 1;
  command "`\pscustom[fillstyle=solid]{'"
    circle rad 2/5 with .c at I.c
    circle rad 1/5 with .c at I.c
    circle rad 0.1 with .c at I.c
  command "`}%'"]' )    

define(`copper',`0.85, 0.54, 0.40')  
  
cct_init

O: (0,0)

_pi=50
_hy=8
_hm=12
_g=2
_h=15
_rn=40
_km=0.5 #
_p=2 #number of poles

radius_ = ((_h/2)/3)*0.90

move to O
#H-axis ticks
{
  for n = 0 to _p-1 do {
    line 1.5 right _pi
    V_tick
    if n== 0 then {
    "$\frac{\pi}{p}$" below
    } else {
    sprintf("$\frac{%g\pi}{p}$",n+1) below
    }
  }
}
line invis up _rn-_h/2-_g-_hm-_hy
#Inner Yoke
{
  command "\pscustom[fillstyle=solid, fillcolor=lightgray, linecolor=white]{"
  line right _p*_pi
  line up _hy color "white"
  line left _p*_pi
  command "}"
}
{line right _p*_pi}
line invis up _hy
{line right _p*_pi}
#Magnets
{
  for n = 0 to _p-1 do {
    if n==0 then {
      #command "\pscustom[fillstyle=crosshatch, linecolor=black]{"
        line up _hm
        line right (1-_km)/2*_pi
        line down _hm
      #command "}"
    } else {
      line invis left (1-_km)/2*_pi    
      #command "\pscustom[fillstyle=crosshatch, linecolor=black]{"
        line up _hm
        line right (1-_km)*_pi
        line down _hm
      #command "}"
    }
    if (-1)^n>0 then {
      #command "\pscustom[fillstyle=vlines, linecolor=black]{"
    } else {
      #command "\pscustom[fillstyle=hlines, linecolor=black]{"
    }
      line up _hm
      line right _km*_pi
      line down _hm
    #command "}"
    line invis right (1-_km)/2*_pi
  }
  line invis left (1-_km)/2*_pi    
  #command "\pscustom[fillstyle=crosshatch, linecolor=black]{"
    line up _hm
    line right (1-_km)/2*_pi
    line down _hm
  #command "}"
}
line invis up _g+_hm
#Stator IR
#{
  #line dashed thick 0.5 right _p*_pi
#}
line invis up _h/2
#{
  #centreline(right_ _p*_pi,,8,1,)
#}
line invis up _h/2
#Stator OR
#{
  #line dashed thick 0.5 right _p*_pi
#}
line invis up _g+_hm
#Outer Yoke IR
{
  line right _p*_pi
}
#Magnets
{
  for n = 0 to _p-1 do {
    if n==0 then {
      #command "\pscustom[fillstyle=crosshatch, linecolor=black]{"
        line down _hm
        line right (1-_km)/2*_pi
        line up _hm
      #command "}"
    } else {
      line invis left (1-_km)/2*_pi    
      #command "\pscustom[fillstyle=crosshatch, linecolor=black]{"
        line down _hm
        line right (1-_km)*_pi
        line up _hm
      #command "}"
    }
    if (-1)^n>0 then {
      #command "\pscustom[fillstyle=vlines, linecolor=black]{"
    } else {
      #command "\pscustom[fillstyle=hlines, linecolor=black]{"
    }
      line down _hm
      line right _km*_pi
      line up _hm
    #command "}"
    line invis right (1-_km)/2*_pi
  }
  line invis left (1-_km)/2*_pi    
  #command "\pscustom[fillstyle=crosshatch, linecolor=black]{"
    line down _hm
    line right (1-_km)/2*_pi
    line up _hm
  #command "}"
}
#Outer Yoke
{
  command "\pscustom[fillstyle=solid, fillcolor=lightgray, linecolor=white]{"
  line right _p*_pi
  line up _hy color "white"
  line left _p*_pi
  command "}"
}
{line right _p*_pi}
line invis up _hy
{line right _p*_pi}

line thick 1.5 from (-_pi/4,0) to (_p*_pi+_pi/4,0) ->; "$\phi$" ljust
line thick 1.5 from (0,-_hm/4) to (0,_rn+_h+_g+_hm+_hy) ->; "$r$" rjust

line thick 1.5 from (-_hm/8, _hm/4) to (_hm/8, _hm/2)
line thick 1.5 colored "white" from (-_hm/8, _hm/4+_hm/16) to (_hm/8, _hm/2+_hm/16)
line thick 1.5 from (-_hm/8, _hm/4+_hm/8) to (_hm/8, _hm/2+_hm/8)

dimension_(from (_p*_pi,0) to (_p*_pi,_rn),-5,"$r_n$",14pt__,,->)
dimension_(from (0,_rn-_h/2) to (0,_rn+_h/2),10,"$h_c$",14pt__,,<->)
dimension_(from (0,_rn-_h/2-_g) to (0,_rn-_h/2),15,"$g\qquad$",14pt__,,,<->)
dimension_(from (0,_rn+_h/2) to (0,_rn+_h/2+_g),15,"$g\qquad$",14pt__,,,<->)
dimension_(from (0,_rn-_h/2-_g-_hm) to (0,_rn-_h/2-_g),10,"$h_{mi}$",14pt__,,<->)
dimension_(from (0,_rn+_h/2+_g) to (0,_rn+_h/2+_g+_hm),10,"$h_{mo}$",14pt__,,<->)
dimension_(from (0,_rn-_h/2-_g-_hm-_hy) to (0,_rn-_h/2-_g-_hm),10,"$h_y$",14pt__,,<->)
dimension_(from (0,_rn+_h/2+_g+_hm) to (0,_rn+_h/2+_g+_hm+_hy),10,"$h_y$",14pt__,,<->)

dimension_(from ((1-_km)/2*_pi,_rn+_h/2+_g+_hm+_hy) to ((1+_km)/2*_pi,_rn+_h/2+_g+_hm+_hy),10,"$k_m\frac{\pi}{p}$",28pt__,,<->)

"\textbf{I}"   at (-30,_rn-_h/2-_g-_hm-_hy/2)
"\textbf{II}"  at (-30,_rn-_h/2-_g-_hm/2)
"\textbf{III}" at (-30,_rn)
"\textbf{IV}"  at (-30,_rn+_h/2+_g+_hm/2)
"\textbf{V}"   at (-30,_rn+_h/2+_g+_hm+_hy/2)

_rcim=_rn-_h/2-_g-_hm/2
_rcom=_rn+_h/2+_g+_hm/2

#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(right_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)
  #command "}"
#] with .c at ((1-_km)/4*_pi,_rcim)
#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(up_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)
  #command "}"
#] with .c at ((1-_km)/2*_pi+_km/2*_pi,_rcim)
#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(left_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)
  #command "}"
#] with .c at ((1-_km)*_pi+_km*_pi,_rcim)
#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(down_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)] with .c at ((1-_km)*_pi+2*_km*_pi,_rcim)
  #command "}"
#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(right_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)
  #command "}"
#] with .c at (7*(1-_km)/4*_pi+2*_km*_pi,_rcim)

#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(left_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)
  #command "}"
#] with .c at ((1-_km)/4*_pi,_rcom)
#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(up_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)
  #command "}"
#] with .c at ((1-_km)/2*_pi+_km/2*_pi,_rcom)
#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(right_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)
  #command "}"
#] with .c at ((1-_km)*_pi+_km*_pi,_rcom)
#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(down_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)
  #command "}"
#] with .c at ((1-_km)*_pi+2*_km*_pi,_rcom)
#[ command "\pscustom[fillstyle=solid, fillcolor=white, linecolor=black]{"
  #darrow(left_ _hm*0.75 ,,,_hm/4,_hm*0.75,_hm/4,|-)
  #command "}"
#] with .c at (7*(1-_km)/4*_pi+2*_km*_pi,_rcom)


start_offset = 5

xi = start_offset+radius_
jump = 11*radius_

max_coils = 3
for i = 0 to max_coils do{ #repeat 3 times
  rgbfill(copper,
    line from (xi-1.5*radius_,_rn-_h/2) to (xi-1.5*radius_,_rn+_h/2)
    line from (xi-1.5*radius_,_rn+_h/2) to (xi+1.5*radius_,_rn+_h/2)
    line from (xi+1.5*radius_,_rn+_h/2) to (xi+1.5*radius_,_rn-_h/2)
    line from (xi+1.5*radius_,_rn-_h/2) to (xi-1.5*radius_,_rn-_h/2)
  )
  #move to O
  I_cross with .c at (xi,_rn)
  I_cross with .c at (xi,_rn+2*radius_)
  I_cross with .c at (xi,_rn-2*radius_)
  
  rgbfill(copper,
    line from (xi+1.5*radius_,_rn+_h/2) to (xi+4.5*radius_,_rn+_h/2)
    line from (xi+4.5*radius_,_rn+_h/2) to (xi+4.5*radius_,_rn-_h/2)
    line from (xi+4.5*radius_,_rn-_h/2) to (xi+1.5*radius_,_rn-_h/2)
  )
  
  I_dot with .c at (xi+3*radius_,_rn)
  I_dot with .c at (xi+3*radius_,_rn+2*radius_)
  I_dot with .c at (xi+3*radius_,_rn-2*radius_)
  
  #if i < max_coils then{
    #line from (xi+4.5*radius_,_rn+_h/2) to (xi+jump+4.5*radius_,_rn+_h/2)
    #line from (xi+4.5*radius_,_rn-_h/2) to (xi+jump+4.5*radius_,_rn-_h/2)
  #}
  
  xi = xi + jump
  }

xi = start_offset+radius_  
#dimension_(from (0,_rn-_h/2) to (0,_rn+_h/2),10,"$h_c$",14pt__,,<->)
dimension_(from (xi+4.5*radius_+jump,_rn) to (xi-1.5*radius_+2*jump,_rn),0,"$k_{c}$",10pt__,,<->)
#dimension_(from (Rect_(ri+hmi+h+hmo,start_ang)) to (Rect_(ri+hmi+h+hmo,start_ang+magnetPitch)),-5,"$k_{m}\frac{\pi}{p}$", dx,0,<->)

#dimension_(from (Rect_(ri+hmi+g,end_ang)) to (Rect_(ri+hmi+g+h,end_ang)),5,"$h$", dx,2,<->)

#dimension_(from (Rect_(ri+hmi+g+h/2,start_ang+coil_side_angle)) to (Rect_(ri+hmi+g+h/2,start_ang+coil_side_angle+coil_block_angle)),0,"", 0,0,<->)
#"$k_{c}$" at 0.75 between last line .end and 2nd last line above

#dimension_(from (Rect_(ri+hmi,start_ang)) to (Rect_(ri+hmi+g,start_ang)),-2,"", dx,0,<->)
#"$g$" at last line .end below ljust  
  
.PE